package com.profit.anton.firstapp.fegures;

/**
 * Created by anton on 11.06.17.
 */
public class Square  extends MainFigure {
    /**
     *
     */
    private double height;


    public Square(double heightone) {
        this.height = heightone;
    }

    /**
     *
     * @return
     */
    public double areaSquare() {
        return Math.pow(height, 2);
    }


    public double getHeight() {
        return height;
    }


    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Square of " + super.toString() + "area " + areaSquare();
    }
}



