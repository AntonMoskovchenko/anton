package com.profit.anton.firstapp.fegures;

/**
 * Created by anton on 11.06.17.
 */
public class Rectangle extends MainFigure {
    /**
     *
     */
    private double weight;
    /**
     *
     */
    private double height;

    public Rectangle(double weight, double height) {
        this.weight = weight;
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    /**
     *
     * @return
     */
    public double areaRectung() {
    return weight * height;
    }

    @Override
    public String toString() {
        return "Rectungl of  " + super.toString() + " area :" + areaRectung();
    }
}
