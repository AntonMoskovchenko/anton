package com.profit.anton.firstapp.fegures;

/**
 * Created by anton on 16.06.17.
 */
public class Cube extends MainFigure {
    /**
     *
     */
    private double weight;
    /**
     *
     */
    private double height;

    public Cube(double weight, double height) {
        this.weight = weight;
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    /**
     *
     * @return
     */
    public double areaCube() {
        return  weight / 2 * height;
    }

    /**
     *
     * @return
     */
    public double volumeCube() {
        return Math.pow(weight, 3);
    }

    @Override
    public String toString() {
        return "Cube of " + super.toString() + " area : " + areaCube() + " volume : " + volumeCube();
    }

}
