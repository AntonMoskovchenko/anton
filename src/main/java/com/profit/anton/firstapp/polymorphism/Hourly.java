package com.profit.anton.firstapp.polymorphism;

/**
 * Created by anton on 17.06.17.
 */
public class Hourly extends Employee {
    /**
     *
     */
    private int hoursWorker;

    /**
     *
     * @param eName
     * @param eAddress
     * @param ePhone
     * @param socSecNumber
     * @param rate
     */
    public Hourly(String eName, String eAddress, String ePhone, String socSecNumber, double rate) {
        super(eName, eAddress, ePhone, socSecNumber, rate);
        hoursWorker = 3;
    }

    /**
     *
     * @param moreHours
     */
    public void addHours(int moreHours) {
        hoursWorker += moreHours;
    }

    /**
     *
     * @return
     */
    @Override
    public double pay() {
        double payment = payRate * hoursWorker;
        hoursWorker = 0;
        return payment;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String result = super.toString();
        result += "\nCurrent hours " + hoursWorker;
        return result;
    }
}
