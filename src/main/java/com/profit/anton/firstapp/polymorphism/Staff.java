package com.profit.anton.firstapp.polymorphism;

import java.util.ArrayList;

/**
 * Created by anton on 17.06.17.
 */
public class Staff {

    /**
     *
     */
    private ArrayList<StaffMember>staffList;

    public Staff() {
        staffList = new ArrayList<>();
        staffList.add(new Executive("Sam", "123 Main Line", "555-556", "123-456798", 2453.85));
        staffList.add(new Employee("Carla", "124 Main Line", "555-557", "123-456799", 1246.15));
        staffList.add(new Employee("Woody", "125 Main Line", "555-558", "123-456800", 1169.23));
        staffList.add(new Hourly("Diana", "126 Main Line", "555-559", "123-456801", 10.55));
        staffList.add(new Volunteer("Norn", "127 Main Line", "555-560"));
        staffList.add(new Volunteer("Cliff", "128 Main Line", "555-561"));

        ((Executive) staffList.get(0)).awardBonus(500.00);
        ((Hourly) staffList.get(3)).addHours(40);
    }

    /**
     *
     */
    public void payDay() {
        double amount;
        for (int count = 0; count < staffList.size(); count++) {
            System.out.println(staffList.get(count));
            amount = staffList.get(count).pay();

            if (amount == 0.0) {
                System.out.println("Thanks");
            } else {
                System.out.println("Paid " + amount);
            }
            System.out.println("----------------------------------");
        }
    }
}
