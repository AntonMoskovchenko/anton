package com.profit.anton.firstapp.polymorphism;

/**
 * Created by anton on 17.06.17.
 */
public class Volunteer extends StaffMember {
    /**
     *
     * @param eName
     * @param eAddress
     * @param ePhone
     */
    public Volunteer(String eName, String eAddress, String ePhone) {
        super(eName, eAddress, ePhone);
    }

    /**
     *
     * @return
     */
    @Override
    public double pay() {
        return 0.0;
    }
}
