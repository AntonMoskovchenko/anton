package com.profit.anton.firstapp.polymorphism;

/**
 * Created by anton on 17.06.17.
 */
abstract class StaffMember {
    /**
     *
     */
    private String name;
    /**
     *
     */
    private String address;
    /**
     *
     */
    private String phone;

    /**
     *
     * @param eName
     * @param eAddress
     * @param ePhone
     */
    protected StaffMember(String eName, String eAddress, String ePhone) {
        this.name = eName;
        this.address = eAddress;
        this.phone = ePhone;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String result = "Name " + name + "\n";
        result += "Adress " + address + "\n";
        result += "Phone " + phone + "\n";
        return result;
    }

    public abstract double pay();
}
