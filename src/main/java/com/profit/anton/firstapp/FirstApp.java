package com.profit.anton.firstapp;

import static java.lang.System.*;

/**
 * Created by profit on 27.05.17.
 */
public class FirstApp {

    /**
     * print method
     */
    public  static <T> void print(T t) {
        out.print(t);

    }

    /**
     * print ln method objects
     */

    public static void println(Object object) {
        out.println(object);
    }
    /**
     * print ln method that prints new line
     */

    public static void println() {
        out.println();
    }

}