package com.profit.anton.firstapp.homework;

/**
 * Created by anton on 08.06.17.
 */
public class SequentialSearch {
    /**
     *
     * @param arr
     * @param key
     * @return
     */
    public static int linerSearch(int[] arr, int key) {

        int size = arr.length;
        for (int i = 0; i < size; i++) {
            if (arr[i] == key) {
                return i;
            }
        }
        return 0;
    }
}
