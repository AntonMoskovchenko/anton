package com.profit.anton.firstapp.conditional;

/**
 * Created by profit on 28.05.17.
 */
public class ConditionalStatements {

    protected ConditionalStatements() {

    }
    /**
     *
     */

    public static void positivOrNegative(Integer x) {

        int input = x;

        if (input > 0) {
            System.out.println("number is positive");

        } else  if (input < 0) {
            System.out.println("Number is negative");

        } else {
            System.out.println("Number is  zero");
        }
    }
}