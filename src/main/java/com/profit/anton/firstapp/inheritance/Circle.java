package com.profit.anton.firstapp.inheritance;

/**
 * Created by anton on 11.06.17.
 */
public class Circle {
    /**
     *
     */
    private double radius = 1.0;

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return calculateArea();
    }

    private double calculateArea() {
        return this.radius * this.radius * Math.PI;
    }

    @Override
    public String toString() {
        return "The radius of the circle is: " + radius + ", and the area is: "
                + calculateArea();
    }
}