package com.profit.anton.firstapp.loopsandarrays;

import java.util.Arrays;

/**
 * Created by anton on 04.06.17.
 */
public class Sorting {
    /**
     * @param num
     */
    ///2134
    public static void selectionSort(int[] num) {

        int first;
        int temp;

        for (int i = num.length - 1; i > 0; i--) {

            first = 0;

            for (int j = 1; j <= i; j++) {

                if (num[j] < num[first]) {
                    first = j;
                }

                temp = num[first];
                num[first] = num[i];
                num[i] = temp;

            }
        }
    }

    /**
     *
     * @param iArr
     */
    public static void sorting(int[] iArr) {


        for (int number : iArr) {
        }

        Arrays.sort(iArr);

    }
}