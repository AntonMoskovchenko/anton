package com.profit.anton.firstapp.loopsandarrays;

import java.util.Arrays;


/**
 * donegjhcxese
 */
public class Foorloop {
    /**
     *
     * @param myArray
     * @param myArraySecond
     */
    public static void equalityTwoArrays(int[] myArray, int[] myArraySecond) {

        boolean equalorNot = true;

        if (myArray.length == myArraySecond.length) {

            for (int i = 0; i < myArray.length; i++) {

                if (myArray[i] != myArraySecond[i]) {
                    equalorNot = false;
                }
            }
        } else {
            equalorNot = false;
        }
        if (equalorNot) {
            System.out.println("Two arrays are equals");
        } else {
            System.out.println("Two arrays are not equals");
        }
    }

    /**
     *
     * @param myArray
     */
    public static void uniquaerValues(int[] myArray) {

        int arraySize = myArray.length;

        System.out.print("Original array : ");

        for (int i = 0; i < arraySize; i++) {
            System.out.print(myArray[i] + "\t");
        }

        for (int i = 0; i < arraySize; i++) {

            for (int j = i + 1; j < arraySize; j++) {

                if (myArray[i] == myArray[j]) {

                    myArray[j] = myArray[arraySize - 1];

                    arraySize--;

                    j--;
                }

            }

        }

        int[] arrayResult = Arrays.copyOf(myArray, arraySize);

        System.out.println();

        System.out.println("Array with unique values : ");

        for (int i = 0; i < arrayResult.length; i++) {
            System.out.print(arrayResult[i] + "\t");
        }
    }
}
