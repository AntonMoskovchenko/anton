package com.profit.anton.firstapp.loopsandarrays;

import static com.profit.anton.firstapp.FirstApp.*;
/**
 * Created by profit on 28.05.17.
 */
public class WhilleLoopsAndArrays {
    /**
     *
     * @param str
     */
    public  static void selectChoice(String str) {

        String choice;
        String con = "y";

        println("What is the command keyword to exit a loop in Java?");

        println("a.quit");
        println("b.continue");
        println("c.break");
        println("d.exit");

     while (con.compareTo("y") == 0) {

         print("Enter your choice : ");

         choice = str;

         if (choice.compareTo("c") == 0) {
             println("Congratulations!");
             break;

         } else if (choice.compareTo("q") == 0 || choice.compareTo("e") == 0) {
             println("Exiting....");
             break;

         } else {
             println("Incorrect");
             break;
         }


     }
    }
}
