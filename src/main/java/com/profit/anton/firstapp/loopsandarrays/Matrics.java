package com.profit.anton.firstapp.loopsandarrays;

/**
 * Created by anton on 03.06.17.
 */
public class Matrics {
    /**
     *
     * @param first
     * @param second
     * @return
     */
    public static Double[][] multiplicar(Double[][] first, Double[][] second) {

        int firstRows = first.length;
        int firstColumns = first[0].length;
        int secondRows = second.length;
        int secondColumns = second[0].length;

        if (firstColumns != secondRows) {
            throw new IllegalArgumentException("First Rows: " + firstColumns
                    + " did not match Second Columns " + secondRows + ".");
        }

        Double[][] result = new Double[firstRows][secondColumns];
        for (int i = 0; i < first.length; i++) {
            for (int j = 0; j < second.length; j++) {
                result[i][j] = 0.00000;
            }
        }

        for (int i = 0; i < firstRows; i++) { // firstRow
            for (int j = 0; j < secondColumns; j++) { // secondColumn
                for (int k = 0; k < result.length; k++) { // resultColumn
                    result[i][j] += first[i][k] * second[k][j];
                }
            }
        }

        return result;
    }
}
