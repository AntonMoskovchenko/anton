package com.profit.anton.firstapp.datatypes;

/**
 * Created by profit on 28.05.17.
 */
public class DataTypes {

    /**
     *
     */

    private byte aByte;
    /**
     *
     */
    private short aShort;
    /**
     *
     */
    private long aLong;
    /**
     *
     */
    private int anInt;
    /**
     *
     */
    private char aChar;
    /**
     *
     */
    private boolean aBoolean;
    /**
     *
     */
    private float aFloat;
    /**
     *
     */
    private double aDouble;

    public DataTypes(byte aByte, short aShort, long aLong, int anInt, char aChar,
                     boolean aBoolean, float aFloat, double aDouble) {
        this.aByte = aByte;
        this.aShort = aShort;
        this.aLong = aLong;
        this.anInt = anInt;
        this.aChar = aChar;
        this.aBoolean = aBoolean;
        this.aFloat = aFloat;
        this.aDouble = aDouble;
    }

    /**
     *
     * @return
     */
    public float getaFloat() {
        return aFloat;
    }

    /**
     *
     * @return
     */
    public double getaDouble() {
        return aDouble;
    }

    /**
     *
     * @return
     */
    public byte getaByte() {
        return aByte;
    }

    /**
     *
     * @return
     */
    public short getaShort() {
        return aShort;
    }

    /**
     *
     * @return
     */
    public long getaLong() {
        return aLong;
    }

    /**
     *
     * @return
     */
    public int getAnInt() {
        return anInt;
    }

    /**
     *
     * @return
     */
    public char getaChar() {
        return aChar;
    }

    /**
     *
     * @return
     */
    public boolean isaBoolean() {
        return aBoolean;
    }
}
