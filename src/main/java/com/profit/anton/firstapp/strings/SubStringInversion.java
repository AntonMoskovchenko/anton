package com.profit.anton.firstapp.strings;

/**
 * Created by anton on 10.06.17.
 */
public class SubStringInversion {

    // str = "abctrefcba", returns = "abc";
    // str = "rmmr", returns : "rm";
    // str = "asasfgh" returns : "";

    /**
     *
     * @param str
     * @return
     */
    public  static String subStringSearch(String str) {

        StringBuilder result = new StringBuilder();

        int length = str.length();

        for (int i = 0; i < length; i++) {
            char symbol = str.charAt(i);
            if (symbol == str.charAt(length - (i + 1))) {
                result.append(symbol);
            } else {
                break;
            }
        }
        return result.toString();
    }
}
