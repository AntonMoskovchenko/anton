package com.profit.anton.firstapp.strings;

import java.util.StringTokenizer;

/**
 * Created by anton on 10.06.17.
 */
public class StringTokenizing {

    /**
     *
     * @param str
     */
    public static void splitBySpace(String str) {

        StringTokenizer st = new StringTokenizer(str);

        System.out.println(".... Split by space .....");
        while (st.hasMoreElements()) {
            System.out.println(st.nextElement());
        }
    }

    /**
     *
     * @param str
     */
    public static void splitByComma(String str) {
        System.out.println(".... Split by comma ',' .....");
        StringTokenizer sttwo = new StringTokenizer(str, ",");

        while (sttwo.hasMoreElements()) {
            System.out.println(sttwo.nextElement());
        }
    }
}
