package com.profit.anton.firstapp.strings;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


/**
 * Created by anton on 10.06.17.
 */
public class Converter {
    /**
     *
     * @param mxlSource
     * @return
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     */
    public static Document stringToDom(String mxlSource) throws SAXException,
            ParserConfigurationException, IOException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        return builder.parse(new InputSource(new StringReader(mxlSource)));
    }

    /**
     *
     * @param doc
     * @return
     * @throws TransformerException
     */
    public String documentToString(org.w3c.dom.Document doc) throws TransformerException {

        //Create dom source for document
        DOMSource domSource = new DOMSource(doc);

        //create string writer
        StringWriter stringWriter = new StringWriter();

        //create resultstream fot the transformer
        StreamResult result = new StreamResult(stringWriter);

        //create the transformer to serialize the document
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        transformer.setOutputProperty("indent", "");

        //transform the document to the result stream
        transformer.transform(domSource, result);
        return stringWriter.toString();
    }

    /**
     *
     * @param filename
     * @return
     */
    public static StringBuilder lineXml(String filename) {

        StringBuilder sb = new StringBuilder();

        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {

            String line;


            while ((line = br.readLine()) != null) {
                sb.append(line.trim());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb;
    }

    /**
     *
     * @param mxlSource
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws TransformerException
     */
    public static void stringToDomFile(String mxlSource) throws SAXException,
            ParserConfigurationException, IOException, TransformerException {

        //parth the given input
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(mxlSource)));

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);

        StreamResult result = new StreamResult(new File("my-file.xml"));
        transformer.transform(source, result);
    }
}
