package com.profit.anton.unzip;


import com.profit.anton.firstapp.unzip.UnzipUtility;

/**

 * A console application that tests the UnzipUtility class
 *
 * @author www.codejava.net
 *
 */
public class UnzipUtilityTest {
	public static void main(String[] args) {
		String zipFilePath = "/home/anton/Project/src/main/resources/MyZIP.zip";
		String destDirectory = "/home/anton/Project/src/test/java/com/profit/anton/unzip";
		UnzipUtility unzipper = new UnzipUtility();
		try {
			unzipper.unzip(zipFilePath, destDirectory);
		} catch (Exception ex) {
			// some errors occurred
			ex.printStackTrace();
		}
	}
}
