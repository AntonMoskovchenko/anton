package com.profit.anton.encapsulation;

import com.profit.anton.firstapp.encapsulation.User;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Created by anton on 17.06.17.
 */
public class UserTest {

    @Test
    public void setPassword() {
        User u = new User();
        u.setPassword("qwerty");
        assertEquals("qwerty", u.getPassword());
    }

    @Test
    public void setPasswordShort() {
        User u = new User();
        u.setPassword("aa");
        assertEquals(null, u.getPassword());
    }

    @Test
    public void setPasswordNull() {
        User u = new User();
        u.setPassword(null);
        assertEquals(null, u.getPassword());
    }

    @Test(expected = NullPointerException.class)
    public void setPasswordNull2() {
        User u = new User();
        u.setPassword(null);
        assertEquals(null, u.getPassword());
    }

    @Test
    public void setLoginIDValid() {
        User u = new User();
        u.setLongID("a123");
        assertEquals("a123", u.getLongID());
    }

    @Test
    public void setLoginIDvalid() {
        User u = new User();
        u.setLongID("@#$%^");
        assertEquals(null, u.getLongID());
    }
}
