package com.profit.anton.loopsandarrays;

import org.junit.Test;

import java.util.Arrays;

import static com.profit.anton.firstapp.loopsandarrays.Sorting.*;

/**
 * Created by anton on 04.06.17.
 */
public class SortingTest {

    @Test
    public void Test() {

        int [] numbers = {2,1,4,3};


        selectionSort(numbers);

        System.out.print(Arrays.toString(numbers));

        System.out.println();

        sorting(numbers);

        System.out.print(Arrays.toString(numbers));

    }
}
