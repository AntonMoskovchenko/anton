package com.profit.anton.pomylorphism;

import com.profit.anton.firstapp.polymorphism.Staff;
import org.junit.Test;

/**
 * Created by anton on 17.06.17.
 */
public class Firm {

    @Test
    public  void poly() {
        Staff personnel = new Staff();
        personnel.payDay();
    }
}
