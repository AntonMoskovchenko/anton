package com.profit.anton.inheritance;

import com.profit.anton.firstapp.fegures.Cube;
import com.profit.anton.firstapp.fegures.Rectangle;
import com.profit.anton.firstapp.fegures.Square;
import org.junit.Test;


/**
 * Created by anton on 11.06.17.
 */
public class FiguresTest {

    @Test
    public  void figureTest() {

        Square square =new Square(5);
        System.out.println(square.toString());

        Rectangle rectangle =new Rectangle(8.6,7);
        System.out.println(rectangle.toString());

        Cube cube =new Cube(10,5);
        System.out.println(cube.toString());

    }
}
