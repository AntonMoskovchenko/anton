package com.profit.anton.inheritance;

import com.profit.anton.firstapp.inheritance.Circle;
import com.profit.anton.firstapp.inheritance.Cylinder;
import org.junit.Test;

/**
 * Created by anton on 11.06.17.
 */
public class Figures {

    @Test
    public void getArea() {
        Circle circle = new Circle();
        System.out.println(circle.toString());

        Circle circle1 =new Circle(4.2);
        System.out.println(circle1.getArea());


    }

    @Test
    public void cylinderData() {
        Cylinder cylinder = new Cylinder();
        System.out.println(cylinder.getHeight());
        System.out.println(cylinder.getRadius());
        System.out.println(cylinder.getArea());
        System.out.println(cylinder.getVolume());
        System.out.println(cylinder.toString());

        System.out.println();
        Cylinder cylinder1 = new Cylinder(3.4);
        System.out.println(cylinder1.getHeight());
        System.out.println(cylinder1.getVolume());
        System.out.println(cylinder1.toString());

        System.out.println();
        Cylinder cylinder2 = new Cylinder(4.5, 6.7);
        System.out.println(cylinder2.getRadius());
        System.out.println(cylinder2.getHeight());
        System.out.println(cylinder2.getArea());
        System.out.println(cylinder2.getVolume());
        System.out.println(cylinder2.toString());
    }
}